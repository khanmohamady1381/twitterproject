package Start;

import database.Connect;
import mainPage.twittPage;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Signup {
    JFrame newUser = new JFrame("SignUp");
    JButton btn_signup = new JButton("Sign Up");
    JTextField userName = new JTextField("Enter Your Name");
    JPasswordField passwordField = new JPasswordField("Eter Your Password");
    private void setUserName(){
        userName.setBounds(300,320,200,40);
        userName.selectAll();
    }
    private void setPasswordField(){
        passwordField.setBounds(300,380,200,40);
        passwordField.setText("");
        passwordField.selectAll();
    }
    private void setBtn_signup(){
    btn_signup.setBounds(300,440,200,50);
    btn_signup.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Connect connect = new Connect();

            try {
                if (connect.isUserExist(userName.getText())){
                    JOptionPane.showMessageDialog(new JFrame(),"This username is already exist.","Error",JOptionPane.ERROR_MESSAGE);

                }else {
                    Connect connect1 = new Connect();

                    connect1.addNewUser(userName.getText(), String.valueOf((passwordField.getPassword())));

                    twittPage newpage = new twittPage();
                    newpage.setMain();
                    newUser.setVisible(false);
                    newUser.dispose();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }

        }
    });}
        public void setNewUser() {
            newUser.setSize(800, 600);
            newUser.setLocationRelativeTo(null);
            newUser.setVisible(true);
            newUser.setContentPane(new JLabel(new ImageIcon("Files/Homepage_cloudimage.jpg")));
            newUser.setLayout(null);
            setBtn_signup();
            newUser.add(btn_signup);
            setUserName();
            setPasswordField();
            newUser.add(userName);
            newUser.add(passwordField);
        }
}