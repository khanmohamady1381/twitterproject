package Start;

import database.Connect;
import mainPage.twittPage;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class logIn {

   private JFrame startPage = new JFrame("Twitter");
   private JTextField nameField = new JTextField("Enter Your username");
   private JPasswordField passField = new JPasswordField("Eneter Your Password");
   private JButton logIn = new JButton("LogIn");
    private void setLogIn() {
        logIn.setBounds(130,245,200,40);
        logIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Connect connect = new Connect();
                if(connect.checkUser(nameField.getText() , String.valueOf(passField.getPassword()))){
                    userInfo userinfo = new userInfo();
                    userinfo.setUserName(nameField.getText());
                    twittPage newpage = new twittPage();
                    newpage.setMain();
                    startPage.setVisible(false);
                    startPage.dispose();
                }else {
                    JOptionPane.showMessageDialog(new JFrame(),"username or password is wrong","Error",JOptionPane.ERROR_MESSAGE);

                }
            }
        });
    }
    private void setNameField() {
        nameField.setBounds(130,135,200,40);
        nameField.selectAll();
        nameField.setOpaque(true);
    }
    private void setPassField() {
        passField.setBounds(130, 195, 200, 40);
        passField.setText("");
        passField.selectAll();
    }
    public void setStartPage(){
        startPage.setContentPane(new JLabel(new ImageIcon("Files/2314905.jpg")));
        startPage.setLocationRelativeTo(null);
        startPage.add(nameField);
        startPage.add(passField);
        startPage.setSize(500,500);
        startPage.setVisible(true);
        startPage.add(logIn);
        setPassField();
        setLogIn();
        setNameField();
        startPage.setLayout(null);
    }
}
