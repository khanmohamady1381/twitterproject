package Start;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FirstPage {

   private JFrame welcome = new JFrame("Twitter");
   private Icon signupIcon = new ImageIcon("Files/icons8-add-user-male-50.png") ;
   private Icon logInIcon = new ImageIcon("Files/icons8-female-profile-50.png") ;

  private JButton logIn_btn = new JButton(logInIcon);
  private JButton signUp_btn = new JButton(signupIcon);

    private void LogIn_btn() {

        logIn_btn.setBounds(260,280,150,45);
        signUp_btn.setBounds(100,280,150,45);
        signUp_btn.setText("Sign Up");
        logIn_btn.setText("LogIn");
        logIn_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                logIn btnclik = new logIn();
                btnclik.setStartPage();
                closeWelcome();
            }
        });
        signUp_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Signup newUser = new Signup();
                newUser.setNewUser();

            }
        });

    }

    public void showWelcome() {

        welcome.setTitle("Welcome");
        welcome.setSize(500,400);
        welcome.setLocationRelativeTo(null);
        LogIn_btn();
        welcome.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        welcome.setVisible(true);
        welcome.setContentPane(new JLabel(new ImageIcon("Files/Twitter-logo-illustration.jpg")));
        welcome.setLayout(null);
        welcome.add(logIn_btn);
        welcome.add(signUp_btn);

    }
    public void closeWelcome(){

    }
}
