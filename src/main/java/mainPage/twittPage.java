package mainPage;

import Start.FirstPage;
import Start.userInfo;
import database.Connect;
import mainPage.bottons.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.ArrayList;

public class twittPage {

   private JFrame main = new JFrame("Twitter");
   private JPanel menu = new JPanel();
   private JPanel twittMessages = new JPanel();
   private JTextArea textArea = new JTextArea();

   private JButton btn_MyProfile = new JButton();
   private JButton btn_newTweet = new JButton("TWEET");
   private JButton btn_Follow = new JButton();
   private JButton btn_Unfollow = new JButton();
   private JButton btn_like  = new JButton();
   private JButton btn_Followers = new JButton();
   private JButton btn_Following = new JButton();
   private JButton btn_Timeline = new JButton();
   private JButton btn_Profile = new JButton();
   private JButton btn_Logout = new JButton();
   private JButton btn_Quit = new JButton();

    public void setBtn_Quit(){
        btn_Quit.setBounds(0,590,250,40);
        btn_Quit.setText("Quit");
        btn_Quit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                main.setVisible(false);
                main.dispose();
            }
        });
        main.add(btn_Quit);
    }

    private void setBtn_Logout(){

        btn_Logout.setBounds(0,540,250,40);
        btn_Logout.setText("Log Out");
        btn_Logout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                main.setVisible(false);
                main.dispose();
                FirstPage mo = new FirstPage();
                mo.showWelcome();
            }
        });
        main.add(btn_Logout);
    }

    private void setBtn_Followers(){

        btn_Followers.setBounds(0,440,250,40);
        btn_Followers.setText("Followers");
        btn_Followers.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Follower follower =new Follower();
                follower.setFollower();
            }
        });
        main.add(btn_Followers);
    }

    private void setBtn_Following(){

        btn_Following.setBounds(0,490,250,40);
        btn_Following.setText("Following");
        btn_Following.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Following following =new Follower();
                following.setFollower();
            }
        });
        main.add(btn_Following);
    }

    public void setBtn_Timeline(){

        btn_Timeline.setBounds(0,300,250,40);
        btn_Timeline.setText("Timeline");
        btn_Timeline.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Timeline timeline = new Timeline();
                timeline.setTimeline();
            }
        });
    }
    private void setBtn_like(){
        btn_like.setBounds(0,390,250,40);
        btn_like.setText("Like");
        btn_like.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Like like = new Like();
                like.setTwittLike();
            }
        });
        main.add(btn_like);
    }

    private void setBtn_Profile(){

        btn_Profile.setBounds(0,345,250,40);
        btn_Profile.setText("Profile");
        btn_Profile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Profile profile = new Profile();
                try {
                    profile.sortTwitts();
                    profile.setTwitt();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
        btn_Profile.setOpaque(false);
        main.add(btn_Profile);
    }

    private void setBtn_Follow(){

        btn_Follow.setBounds(0,255,250,40);
        btn_Follow.setText("Follow");
        btn_Follow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Follow user = new Follow();
                user.setFollow();
            }
        });
    }

    private void setBtn_Unfollow(){

        btn_Unfollow.setBounds(0,300,250,40);
        btn_Unfollow.setText("UnFollow");
        btn_Unfollow.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Follow user = new Unfollow();
                user.setFollow();
            }
        });
    }

    private void setTwittMessages(){

        ArrayList<String> twittsHistory = new ArrayList<>(7);
        ArrayList<String> twittsTime = new ArrayList<>(7);
        int tweetLikes = 0 ;

        Connect connection = new Connect();
        connection.insert_twitts(userInfo.username(),twittsHistory,twittsTime ,tweetLikes);

        twittMessages.setBounds(252,0,650,610);
//        BorderLayout ms = new BorderLayout();
//        twittMessages.setLayout(ms);
        twittMessages.setOpaque(false);
        twittMessages.setBackground(Color.magenta);

        textArea.setBounds(250,0,400,400);
        textArea.setFont(new Font("Playfair Display",Font.BOLD,15));
        textArea.setEditable(false);
        textArea.setOpaque(false);


        for (int i = twittsHistory.size()-1 ; i >= 0 ;i--){
            textArea.append(twittsTime.get(i) +":");
            textArea.append(twittsHistory.get(i));
            textArea.append("\n\n\n");
            textArea.setOpaque(false);
        }

        twittMessages.add(textArea);
//        twittMessages.add(new JLabel(new ImageIcon("/home/mohamad/Desktop/Twitter/Files/563945_100-best-free-backgrounds-for-logo-presentations_1300x858_h.jpg")));
//        JScrollPane text = new JScrollPane(textArea);
//        text.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//        text.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
//        text.getViewport();
//        twittMessages.add(text);
    }

    private void setBtn_newTweet() {
        btn_newTweet.setBounds(250, 608, 650, 62);
        btn_newTweet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Tweets addNewTweet = new Tweets();
                addNewTweet.setFram_Tweet();

            }

        });

    }
    private void setBtn_MyProfile(){

        btn_MyProfile.setBounds(0,210,250,40);
        btn_MyProfile.setText("My Profile");
        btn_MyProfile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Profile myProfile = new MyProfile();
                try {
                    myProfile.setTwitt();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
    }

    private void setMenu(){
        menu.setBounds(0,0,250,670);
        JLabel background = new JLabel(new ImageIcon("Files/82527940-menu-over-dusty-blank-chalkboard-or-blackboard-background-with-room-for-copy-space-.jpg"));
//        menu.add(background);
    }

    public void setMain(){
        main.setSize(900,700);
        main.setLocationRelativeTo(null);
        main.setContentPane(new JLabel(new ImageIcon("Files/134116900-abstract-colorful-background-design-background.jpg")));
        setBtn_newTweet();
        setMenu();
        setBtn_MyProfile();
        setTwittMessages();
        setBtn_Follow();
        setBtn_Unfollow();
        setBtn_Profile();
        setBtn_like();
        setBtn_Timeline();
        setBtn_Followers();
        setBtn_Following();
        setBtn_Logout();
        setBtn_Quit();
        main.add(twittMessages);
        main.add(btn_Timeline);
        main.add(btn_Unfollow);
        main.add(btn_Follow);
        main.add(btn_newTweet);
        main.add(menu);
        main.add(btn_MyProfile);
        twittMessages.setOpaque(false);
        main.setLayout(null);
        main.setVisible(true);
        main.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
