package mainPage.bottons;

import Start.userInfo;
import database.Connect;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.TreeMap;

public class Profile {

    private JFrame twitt = new JFrame();
    private JTextArea twitt_txt = new JTextArea();
    protected   Connect connect = new Connect();
    protected ArrayList<String> follows = new ArrayList<>();
    protected TreeMap<Timestamp,String> twitts = new TreeMap<>();

    private void setTwitt_txt() {

//        List<String> time = new ArrayList<String>(Integer.parseInt(String.valueOf(twitts.keySet())));
        twitt_txt.setBounds(0, 0, 600, 600);
//        for(Timestamp m :(twitts.keySet())){
//            twitt_txt.append(m +"   "+":" +twitts.get(m)+"\n\n\n");
//        }
        twitt_txt.setOpaque(false);
        twitt_txt.setEditable(false);
        twitt_txt.setLineWrap(true);
        twitt_txt.setWrapStyleWord(true);
        twitt.add(twitt_txt);

        JScrollPane text = new JScrollPane(twitt_txt);
        text.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        text.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        text.getViewport();
        twitt.add(text);
    }


    protected void follows() throws SQLException {
        connect.findfollowing(userInfo.username(), follows);
    }

    private void gettwitts()  {
        Connect connect = new Connect();
        connect.insert_follows_twitts(follows, twitts,twitt_txt);
    }

    public void sortTwitts() throws SQLException {
        twitts.keySet().stream().sorted();
    }

    public void setTwitt() throws SQLException {
        twitt.setSize(600,600);
        twitt.setLayout(new BorderLayout());
        follows();
        gettwitts();
        setTwitt_txt();
        twitt.setVisible(true);
    }
}
