package mainPage.bottons;

import database.Connect;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Timeline {

    private JFrame timeline = new JFrame();
    private JTextField name_txt = new JTextField();
    private JButton show_btn =new JButton();
    private JTextArea userTwitts = new JTextArea();


    private void setName(){
        name_txt.setBounds(80,100,200,40);
        name_txt.setText("Enter name to show twitts");
        name_txt.selectAll();
        timeline.add(name_txt);
    }

    private void setUserTwitts(){
        JFrame showMessages = new JFrame();
        showMessages.setSize(600,600);

        userTwitts.setBounds(0, 0, 500, 500);
        userTwitts.setOpaque(false);
        userTwitts.setEditable(false);
        userTwitts.setLineWrap(true);
        userTwitts.setWrapStyleWord(true);
        showMessages.add(userTwitts);

        JScrollPane text_sroll = new JScrollPane(userTwitts);
        text_sroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        text_sroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        text_sroll.getViewport();
        showMessages.add(text_sroll);
        showMessages.setVisible(true);
        showMessages.setLayout(null);
    }

    private void setShow_btn(){
        show_btn.setBounds(80,170,200,40);
        show_btn.setText("Show Twitts");
        show_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Connect connect = new Connect();
                try {
                    if (connect.isUserExist(name_txt.getText())){
                        connect.insert_follows_twitts(name_txt.getText(),userTwitts);
                        setUserTwitts();
                    }else {
                        JOptionPane.showMessageDialog(new JFrame(),"username is wrong","Error",JOptionPane.ERROR_MESSAGE);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });
        timeline.add(show_btn);
    }

    public void setTimeline(){
        timeline.setSize(350,350);
        timeline.setContentPane(new JLabel(new ImageIcon("Files/af8d63a477078732b79ff9d9fc60873f.jpg")));
        setName();
        setShow_btn();
        timeline.setLayout(null);
        timeline.setVisible(true);
    }
}
