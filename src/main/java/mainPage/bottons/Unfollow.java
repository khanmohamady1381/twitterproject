package mainPage.bottons;

import Start.userInfo;
import database.Connect;

public class Unfollow extends Follow {

    @Override
    public void setFollow_btn() {
        super.setFollow_btn();
        super.follow_btn.setText("Unfollow");
        super.follow_btn.addActionListener(actionEvent -> {
            Connect connect = new Connect();
            connect.unfollow(userInfo.userName,super.username_txt.getText());
        } );
    }
}
