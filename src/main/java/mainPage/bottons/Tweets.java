package mainPage.bottons;

import Start.userInfo;
import database.Connect;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import static java.lang.StrictMath.abs;


public class Tweets {

    private int tweetID ;

    private JFrame frame_Tweet = new JFrame();
    private JTextArea txt_Tweet = new JTextArea();
    private JButton btn_sendTweet = new JButton();


    private void gen_tweetID(){
            Random random = new Random();
            tweetID =abs(random.nextInt(500000000));
    }

    private void setBtn_sendTweet(){

        btn_sendTweet.setText("Send It");
        btn_sendTweet.setBounds(210,340,100,40);
        btn_sendTweet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Connect connection = new Connect();
                if (txt_Tweet.getText().length()>=140){
                    JOptionPane.showMessageDialog(new JFrame(),"Your twitt is too long!!","Error",JOptionPane.ERROR_MESSAGE);
                    txt_Tweet.setText("Enter Your New Tweet ");
                }else {
                    connection.addNewTweet(userInfo.userName, txt_Tweet.getText(), tweetID);
                    close_frame();

                }
            }
        });
    }

    private void setTxt_Tweet(){
        txt_Tweet.setText("Enter Your New Tweet ");
        txt_Tweet.setBounds(120,120,300,200);
        txt_Tweet.selectAll();
        txt_Tweet.setLineWrap(true);
        txt_Tweet.setWrapStyleWord(true);
        txt_Tweet.setBorder(BorderFactory.createLineBorder(new Color(179, 175, 175,100),2));
    }

    public void setFram_Tweet(){
        frame_Tweet.setSize(600,600);
        frame_Tweet.setLayout(null);
        frame_Tweet.setVisible(true);
        frame_Tweet.setContentPane(new JLabel(new ImageIcon("Files/whatsapp.jpg")));
        setBtn_sendTweet();
        frame_Tweet.add(btn_sendTweet);
        gen_tweetID();
        setTxt_Tweet();
        frame_Tweet.add(txt_Tweet);
    }
    public void close_frame(){
        frame_Tweet.setVisible(false);
        frame_Tweet.dispose();
    }
}
