package mainPage.bottons;

import Start.userInfo;
import database.Connect;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

public class Follow {

    private JFrame follow = new JFrame("Follow");
    protected JButton follow_btn = new JButton();
    protected JTextField username_txt = new JTextField();

    private void setUsername_txt(){
        username_txt.setBounds(130,150,150,30);
    }

    protected void setFollow_btn(){
        follow_btn.setBounds(130,200,150,40);
        follow_btn.setText("Submit");
        follow_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String userName = username_txt.getText();
                Connect connect = new Connect();
                try {
                    if (username_txt.getText().equals(userInfo.username())){
                        JOptionPane.showMessageDialog(new JFrame(),"you cant follow yourself !!!! ");
                    }
                    if (connect.isUserExist(userName)){
                        Connect connect1 = new Connect();
                        if (connect1.follow(userInfo.userName, username_txt.getText()))
                        {
                            JOptionPane.showMessageDialog(new JFrame(),"you have followed "+username_txt.getText());


                        }else {

                        }
                        follow.setVisible(false);
                        follow.dispose();

                    }else {



                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
            }
        });
    }
    public void setFollow(){
        follow.setSize(400,400);
        follow.setContentPane(new JLabel(new ImageIcon("Files/563945_100-best-free-backgrounds-for-logo-presentations_1300x858_h.jpg")));
        follow.setVisible(true);
        follow.setLayout(null);
        setUsername_txt();
        setFollow_btn();
        follow.add(username_txt);
        follow.add(follow_btn);
    }
}
