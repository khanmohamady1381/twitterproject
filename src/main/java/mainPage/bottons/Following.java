package mainPage.bottons;

import Start.userInfo;
import database.Connect;

import javax.swing.*;
import java.util.ArrayList;

public class Following {

    private JFrame follower = new JFrame();
    protected JTextArea showFollowers = new JTextArea();
    protected ArrayList<String>  followers = new ArrayList<>();

    private void setShowFollowers(){

        showFollowers.setBounds(0, 0, 500, 500);
        showFollowers.setOpaque(false);
        showFollowers.setEditable(false);
        showFollowers.setLineWrap(true);
        showFollowers.setWrapStyleWord(true);
        follower.add(showFollowers);


        JScrollPane text_sroll = new JScrollPane(showFollowers);
        text_sroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        text_sroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        text_sroll.getViewport();
        follower.add(text_sroll);
        follower.setVisible(true);
        follower.setLayout(null);

    }

    protected void getfollowing(){

        Connect connect = new Connect();
        connect.findfollowing(userInfo.username(),followers);
        System.out.println(followers);

        for (String i : followers){
            showFollowers.append(i+"  and");
        }
        showFollowers.append("  are Following you");
    }

    public void setFollower(){
        follower.setSize(300,300);
        getfollowing();
        setShowFollowers();
        follower.setVisible(true);
        follower.setLayout(null);
    }
}

