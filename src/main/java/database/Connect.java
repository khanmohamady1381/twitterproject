package database;

import Start.userInfo;

import javax.swing.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

public class Connect  {

    private String url = null;
    private String query = null;
    private Connection connect = null;
    private Statement state = null;

        public Connect(){
            try {
                Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
                url = "jdbc:mysql://localhost:3306/Twitter?user=root";
                connect = DriverManager.getConnection(url);

                try {
                    state = connect.createStatement();
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }

            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        public void addNewUser(String name , String password) {
            try
                {
                    query = "insert into UserInfo(name,password) values('%s','%s')";
                    query = String.format(query, name, password);
                    state.execute(query);
                } catch (SQLException  throwables) {
                        throwables.printStackTrace();
                }
            closeUp();
        }

        public Boolean checkUser(String name , String password){
            try
            {
                String query = "select * from UserInfo";
                ResultSet result = state.executeQuery(query);
                while (result.next()){
                    String str = result.getNString(1);
                    String pass = result.getString(2);
                    if (name.equals(str) && pass.equals(password)){
                        userInfo newuser = new userInfo();
                        newuser.setUserName(name);
                        return true;
                    }
                    else {
                    }
                }
        } catch ( SQLException throwables) {
        throwables.printStackTrace();
    }
            closeUp();
            return false;
        }

    public void addNewTweet(String name , String tweet , int tweetID ) {
            try

            {
                query = "insert into tweets(name,tweet,tweet_ID,tweet_Time) values('%s','%s','%s','%s')";
                query = String.format(query, name,tweet, String.valueOf(tweetID), LocalDateTime.now());
                state.execute(query);
                    } catch ( SQLException throwables) {
                            throwables.printStackTrace();
                }
            closeUp();
        }

    public Boolean isUserExist(String userName) throws SQLException {

        try {


            String query = "select * from UserInfo";
            ResultSet result = state.executeQuery(query);
            while (result.next()){
                if (userName.equals(result.getNString(1))){
                    return true;
                }
                    else {
                    }
            }} catch ( SQLException throwables) {
                    throwables.printStackTrace();
                }
                closeUp();
                return false;
    }



    public void insert_twitts(String username, ArrayList<String> twittsHistory , ArrayList<String> twittsTime,int twittLikes){
        try
        {
            query = "select * from tweets";
            ResultSet result = state.executeQuery(query);
            while (result.next()){
                if (result.getNString(1).equals(username)) {
                    String str = result.getNString(2);
                    twittsHistory.add(str);
                    twittsTime.add(result.getDate(4) +"  "+result.getTime(4));
                }else {
                }
            }
            Collections.reverse(twittsHistory);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        closeUp();
    }

    public Boolean follow(String user,String follow){
            Boolean flag = false;
        try {

            query = "select * from follow";
            ResultSet result = state.executeQuery(query);
            while (result.next()) {
                String is_follow = result.getNString(2);
                if (userInfo.username().equals(is_follow)) {
                    JOptionPane.showMessageDialog(new JFrame(),"You have been followed him before!!!","Error",JOptionPane.ERROR_MESSAGE);
                    flag = true;
                    return flag;


                } else {
                }
        }
            if (flag==false) {
                String query1 = "insert into follow(user,followers) values('%s','%s')";
                query1 = String.format(query1, follow, user);
                state.execute(query1);
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return flag;
    }
    
    public void unfollow(String user,String follow){

        String query = "DELETE FROM follow where followers='%s'";
        query = String.format(query, follow);
        try {
            state.execute(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        closeUp();

    }

    public void findfollowing(String username, ArrayList<String> follows)  {

        try {
            query = "select * from follow";
            ResultSet result = state.executeQuery(query);
            while (result.next()) {
                String is_follow = result.getNString(2);
                if (username.equals(is_follow)) {
                    follows.add(result.getNString(1));
                } else {

                }

        }} catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        follows.add(userInfo.username());
        closeUp();
        }

        public void findFollowers(String username, ArrayList<String> follows)  {

            try {
                query = "select * from follow";
                ResultSet result = state.executeQuery(query);
                while (result.next()) {
                    String is_follow = result.getNString(1);
                    if (username.equals(is_follow)) {
                        follows.add(result.getNString(2));
                    } else {
                    }
                }} catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            follows.add(userInfo.username());
            closeUp();
        }

    public void insert_follows_twitts(ArrayList<String> follows, TreeMap<Timestamp, String> twittHistory, JTextArea twitt_txt) {
        try {
        query = "select * from tweets";
        ResultSet result = null;
        result = state.executeQuery(query);
            while (result.next()) {
                for (int i = 0 ; i <= follows.size()-1 ; i++){

                String is_follow = null;
                is_follow = result.getNString(1);
                if (follows.get(i).equals(is_follow)) {
                    twitt_txt.append("("+String.valueOf(result.getTimestamp(4))+") "+" "+result.getNString(1)+" :  "+result.getNString(2)+"   "+"    "
                            +"(likes:" + String.valueOf(result.getInt(5))+")"+"(twittID = "+result.getNString(3)+" )"+"\n\n");
//                    twitt_txt.append(String.valueOf(result.getTimestamp(4))+"   "+result.getNString(2)+"   "+result.getNString(1)+"    "
//                            +"(likes:" + String.valueOf(result.getInt(5))+")"+"\n\n");
//                    System.out.println(String.valueOf(result.getNString(2)));
//                    twittHistory.put(result.getTimestamp(4), result.getNString(2));

                } else {
                }
            }}}catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        closeUp();
        }

        public Boolean likeTwitt(String twittID){
            int likes =0;
                try {
                    query = "select * from tweets";
                    ResultSet result = null;
                    result = state.executeQuery(query);
                    while (result.next()){
                        if (twittID.equals(result.getNString(3))){
                              likes = result.getInt(5);
                         }
                    }
                String query1 ="update tweets set likes=%s where tweet_ID='%s'";
                query1=String.format(query1,++likes, twittID);
                state.execute(query1);
            }catch (SQLException e){
                e.printStackTrace();
            }
                closeUp();
                return null;
            }

    public void insert_follows_twitts(String name, JTextArea twitt_txt)  {
        try {
            query = "select * from tweets";
            ResultSet result = null;
            result = state.executeQuery(query);
            while (result.next()) {
                if (name.equals(result.getNString(1))){
                    twitt_txt.append("("+String.valueOf(result.getTimestamp(4))+") "+" "+result.getNString(1)+" :  "+result.getNString(2)+"   "+"    "
                                +"(likes:" + String.valueOf(result.getInt(5))+")"+"\n\n");
                    } else {
                    }
                }}catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        closeUp();
    }

    private void closeUp(){
        try {
            state.close();
            connect.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
















